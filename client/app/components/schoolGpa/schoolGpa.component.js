import template from './schoolGpa.template.html';
import controller from './schoolGpa.controller';
import './schoolGpa.style.css';
import 'angular-ui-grid/ui-grid.min.css';

let schoolGpaComponent = {
  bindings: {},
  template,
  controller
};

export default schoolGpaComponent;
