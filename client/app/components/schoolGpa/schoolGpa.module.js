import angular from 'angular';
import uiRouter from 'angular-ui-router';
import uiGrid from 'angular-ui-grid';
import schoolGpaComponent from './schoolGpa.component';
import schoolGpaService from './schoolGpa.service';

let schoolGpaModule = angular.module('schoolGpa', [
  uiRouter, uiGrid, 'ui.grid.infiniteScroll',
]).config(($stateProvider) => {
  "ngInject";
  $stateProvider.state('schoolGpa', {url: '/schoolGpa', component: 'schoolGpa'});
}).component('schoolGpa', schoolGpaComponent).service('schoolGpaService', schoolGpaService).name;

export default schoolGpaModule;
