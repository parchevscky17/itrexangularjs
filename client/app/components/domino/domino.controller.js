class DominoController {
  constructor() {
    this.createNew = false;
    this.chooseTopPart = false;
    this.currentSpeedRotation = 3;
    this.currentSizeDomino = 10;
    this.availableData = [{id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}, {id: 6}];
    this.viewDomino = [
      {id: 1, class: 'main-part-domino', value: 1}, {id: 2, class: 'main-part-domino', value: 6},
    ];
  }

  rotateLeft(event) {
    event.preventDefault();
    const dominoItem = document.querySelector('.domino-wrapper');
    if (dominoItem) {
      dominoItem.classList.remove("rotate-left-animation");
      dominoItem.classList.remove("rotate-right-animation");
      void dominoItem.offsetWidth;
      dominoItem.classList.add("rotate-left-animation");
    }
  }

  rotateRight(event) {
    event.preventDefault();
    const dominoItem = document.querySelector('.domino-wrapper');
    if (dominoItem) {
      dominoItem.classList.remove("rotate-left-animation");
      dominoItem.classList.remove("rotate-right-animation");
      void dominoItem.offsetWidth;
      dominoItem.classList.add("rotate-right-animation");
    }
  }

  changeSizeDomino(data) {
    const dominoItem = document.querySelector('.domino-wrapper');
    dominoItem.style.fontSize = `${data}px`;
  }

  changeSpeedRotation(data) {
    const dominoItem = document.querySelector('.domino-wrapper');
    dominoItem.style.WebkitAnimationDuration = `${data}s`;
    dominoItem.style.animationDuration = `${data}s`;
  }

  createNewDomino() {
    this.createNew = true;
    this.chooseTopPart = true;
    this.currentSizeDomino = 10;
    this.changeSizeDomino(this.currentSizeDomino);
    this.currentSpeedRotation = 3;
    this.changeSpeedRotation(this.currentSpeedRotation);
    const dominoItem = document.querySelector('.domino-wrapper');
    if (dominoItem) {
      dominoItem.classList.remove("rotate-right-animation");
      dominoItem.classList.remove("rotate-left-animation");
      void dominoItem.offsetWidth;
    }
  }

  chooseFaceDomino(id) {
    if (this.chooseTopPart) {
      this.chooseTopPart = false;
      this.viewDomino[0].value = id;
    } else {
      this.viewDomino[1].value = id;
      this.createNew = false;
    }
  }
}

export default DominoController;
